<?php
/**
 * Setup program
 * Optional
 * - Modify the Config values present in the program_config table
 *
 * @package Example module
 */

DrawHeader( ProgramTitle() ); // Display main header with Module icon and Program title.

if ( $_REQUEST['modfunc'] === 'update'
	&& AllowEdit() ) // AllowEdit must be verified before inserting, updating, deleting data.
{
	if ( isset( $_REQUEST['values'] ) )
	{
		// Verify value is numeric or empty.
		if ( empty( $_REQUEST['values']['EXAMPLE_CONFIG'] )
			|| is_numeric( $_REQUEST['values']['EXAMPLE_CONFIG'] ) )
		{
			// Save value.
			ProgramConfig( 'example', 'EXAMPLE_CONFIG', $_REQUEST['values']['EXAMPLE_CONFIG'] );

			// Add note.
			$note[] = button( 'check' ) . '&nbsp;' .
				dgettext( 'Example', 'The configuration value has been modified.' );
		}
		else // If value is not numeric.
		{
			// Add error message.
			$error[] = _( 'Please enter valid Numeric data.' );
		}
	}

	// Unset modfunc & values & redirect URL.
	RedirectURL( [ 'modfunc', 'values' ] );
}

// Display Setup value form.
if ( empty( $_REQUEST['modfunc'] ) )
{
	// Display note if any.
	echo ErrorMessage( $note, 'note' );

	// Display errors if any.
	echo ErrorMessage( $error, 'error' );

	// Form used to send the updated Config to be processed by the same script (see at the top).
	echo '<form action="' .
		// Always escape URL whether in forms or in links.
		URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=update' ) .
		'" method="POST">';

	// Display secondary header with Save button (aligned right).
	DrawHeader( '', SubmitButton() ); // SubmitButton is only displayed if AllowEdit.

	echo '<br />';

	// Encapsulate content in PopTable.
	PopTable( 'header', dgettext( 'Example', 'Example module Setup' ) );

	// Display the program config options.
	echo '<fieldset><legend>' . dgettext( 'Example', 'Example' ) . '</legend><table>';

	echo '<tr style="text-align:left;"><td>' .
		TextInput(
			ProgramConfig( 'example', 'EXAMPLE_CONFIG' ),
			'values[EXAMPLE_CONFIG]',
			dgettext( 'Example', 'Example config value label' ),
			'type="number" min=0' // Number input with a minimum value of 0 (default step: 1).
		) . '</td></tr>';

	echo '</table></fieldset>';

	// Close PopTable.
	PopTable( 'footer' );

	// SubmitButton is only displayed if AllowEdit.
	echo '<br /><div class="center">' . SubmitButton() . '</div></form>';
}

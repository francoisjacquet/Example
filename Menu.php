<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the Example module
 * - Add Menu entries to other modules
 *
 * @package Example module
 */

/**
 * Use dgettext() function instead of _() for Module specific strings translation
 * see locale/README file for more information.
 */
$module_name = dgettext( 'Example', 'Example' );

// Menu entries for the Example module.
$menu['Example']['admin'] = [ // Admin menu.
	'title' => dgettext( 'Example', 'Example' ),
	'default' => 'Example/ExampleWidget.php', // Program loaded by default when menu opened.
	'Example/ExampleWidget.php' => dgettext( 'Example', 'Example Widget' ),
	1 => _( 'Setup' ), // Add sub-menu 1 (only for admins).
	'Example/Setup.php' => _( 'Configuration' ), // Reuse existing translations: 'Configuration' exists under School module.
] + issetVal( $menu['Example']['teacher'], [] ); // Add entries added by another add-on which was loaded before.

$menu['Example']['teacher'] = [ // Teacher menu
	'title' => dgettext( 'Example', 'Example' ),
	'default' => 'Example/ExampleWidget.php', // Program loaded by default when menu opened.
	'Example/ExampleWidget.php' => dgettext( 'Example', 'Example Widget' ),
] + issetVal( $menu['Example']['teacher'], [] ); // Add entries added by another add-on which was loaded before.

$menu['Example']['parent'] = $menu['Example']['teacher'] + issetVal( $menu['Example']['parent'], [] ); // Parent & student menu.

// Add a Menu entry to the Resources module.
if ( $RosarioModules['Resources'] ) // Verify Resources module is activated.
{
	$menu['Resources']['admin'][] = dgettext( 'Example', 'Example' ); // Add sub-menu 1.
	$menu['Resources']['admin']['Example/ExampleResource.php'] = dgettext( 'Example', 'Example Resource' );
}

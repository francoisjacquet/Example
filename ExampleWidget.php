<?php
/**
 * ExampleWidget.php file
 * Optional
 * - Example of Search Widget and PDF printing
 *
 * @package Example module
 */

// Include Substitutions Program functions.
require_once 'ProgramFunctions/Substitutions.fnc.php';

if ( $_REQUEST['modfunc'] === 'save' ) // Print PDF.
{
	// If students selected, continue.
	if ( ! count( $_REQUEST['st_arr'] ) )
	{
		// Use BackPrompt to display errors when printing PDF: will close the opened browser tab.
		BackPrompt( _( 'You must choose at least one student.' ) );
	}

	// Cast st_arr values to integer to prevent hacking and SQL error.
	$st_list = implode( ',', array_map( 'intval', $_REQUEST['st_arr'] ) );

	// Restrict student list to selected students.
	$extra['WHERE'] = " AND s.STUDENT_ID IN (" . $st_list . ")";

	// Get Marking Period information.
	$mp_RET = DBGet( "SELECT TITLE,END_DATE
		FROM school_marking_periods
		WHERE MP='QTR'
		AND MARKING_PERIOD_ID='" . UserMP() . "'" );

	// Get School information.
	$school_info_RET = DBGet( "SELECT TITLE,PRINCIPAL
		FROM schools WHERE ID='" . UserSchool() . "'
		AND SYEAR='" . UserSyear() . "'" );

	// Order by Grade levels.
	$extra['SELECT'] = ",(SELECT SORT_ORDER
		FROM school_gradelevels
		WHERE ID=ssm.GRADE_ID) AS SORT_ORDER";

	$extra['ORDER_BY'] = 'SORT_ORDER DESC,FULL_NAME';

	// Get Teacher information.
	$extra['SELECT'] .= ",(SELECT " . DisplayNameSQL( 'st' ) . "
		FROM staff st,course_periods cp,schedule ss
		WHERE st.STAFF_ID=cp.TEACHER_ID
		AND cp.COURSE_PERIOD_ID=ss.COURSE_PERIOD_ID
		AND ss.STUDENT_ID=s.STUDENT_ID
		AND ss.SYEAR='" . UserSyear() . "'
		AND ss.MARKING_PERIOD_ID IN (" . GetAllMP( 'QTR', GetCurrentMP( 'QTR', DBDate(), false ) ) . ")
		AND (ss.START_DATE<='" . DBDate() . "'
			AND (ss.END_DATE>='" . DBDate() . "'
				OR ss.END_DATE IS NULL)) LIMIT 1) AS TEACHER";

	// Get student list.
	$student_RET = GetStuList( $extra );

	// PDF options.
	$no_margins = [ 'top' => 0, 'bottom' => 0, 'left' => 0, 'right' => 0 ];

	$handle = PDFStart( [ 'css' => false, 'margins' => $no_margins ] ); // Start PDF buffer.

	$_SESSION['orientation'] = 'landscape';

	$first = true;

	// Loop over the returned students array.
	foreach ( (array) $student_RET as $student )
	{
		if ( ! $first )
		{
			// Page break before new student.
			echo '<div style="page-break-after: always;"></div>';
		}
		else
			$first = false;

		echo '<br /><br /><table style="margin:0 auto; height:77%;">';

		// Format TEXTAREA content.
		// Here we use $_POST exceptionnaly so single quotes are not escaped.
		$subject_text = nl2br( $_POST['subject_text'] );

		$substitutions = [
			'__FULL_NAME__' => $student['FULL_NAME'],
			'__LAST_NAME__' => $student['LAST_NAME'],
			'__FIRST_NAME__' => $student['FIRST_NAME'],
			'__MIDDLE_NAME__' => $student['MIDDLE_NAME'],
			'__SCHOOL_ID__' => $school_info_RET[1]['TITLE'],
			'__GRADE_ID__' => $student['GRADE_ID'],
			'__SUBJECT__' => $_REQUEST['subject'],
		];

		// Apply the substitutions.
		$subject_text = SubstitutionsTextMake( $substitutions, $subject_text );

		// Generate the PDF content.
		echo '<tr><td><span style="font-size:xx-large;">' . $subject_text .
			'</span></td></tr></table>';

		echo '<br /><br /><table style="margin:0 auto; width:80%;">';

		echo '<tr><td><span style="font-size:x-large;">' . $student['TEACHER'] . '</span><br />
			<span style="font-size:large;">' . _( 'Teacher' ) . '</span></td>';

		echo '<td><span style="font-size:x-large;">' . $mp_RET[1]['TITLE'] . '</span>
			<br /><span style="font-size:large;">' . _( 'Marking Period' ) . '</span></td></tr>';

		echo '<tr><td><span style="font-size:x-large;">' . $school_info_RET[1]['PRINCIPAL'] . '</span><br />
			<span style="font-size:large;">' . _( 'Principal' ) . '</span></td>';

		echo '<td><span style="font-size:x-large;">' . ProperDate( $mp_RET[1]['END_DATE'] ) . '</span><br />
			<span style="font-size:large;">' . _( 'Date' ) . '</span></td></tr>';

		echo '</table>';
	}

	PDFStop( $handle ); // Send PDF buffer to impression.
}

// Display Search or list of students.
if ( empty( $_REQUEST['modfunc'] ) )
{
	DrawHeader( ProgramTitle() ); // Display main header with Module icon and Program title.

	// If student list.
	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		// Form used to send the students list
		// and the text to be processed by the same script (see at the top).
		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] .
			'&modfunc=save&include_inactive=' . issetVal( $_REQUEST['include_inactive'], '' ) .
			'&_ROSARIO_PDF=true' ) . '" method="POST">'; // _ROSARIO_PDF=true enables PDF printing.

		// The $extra variable contains the options for the Search function & the extra headers
		// SubmitButton is diplayed only if AllowEdit.
		$extra['header_right'] = SubmitButton( dgettext( 'Example', 'Create Subject PDF for Selected Students' ) );

		$extra['extra_header_left'] = '<table><tr><td>' . TextAreaInput(
			'',
			'subject_text',
			_( 'Text' ),
			'',
			false,
			'text'
		) . '</td></tr>';

		// Substitutions list.
		$substitutions = [
			'__FULL_NAME__' => _( 'Display Name' ),
			'__LAST_NAME__' => _( 'Last Name' ),
			'__FIRST_NAME__' => _( 'First Name' ),
			'__MIDDLE_NAME__' => _( 'Middle Name' ),
			'__SCHOOL_ID__' => _( 'School' ),
			'__GRADE_ID__' => _( 'Grade Level' ),
		];

		$extra['extra_header_left'] .= '<tr><td>' . SubstitutionsInput( $substitutions ) . '</td></tr></table>';
	}

	// If not printing page in PDF.
	if ( ! isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		$extra['SELECT'] = ",s.STUDENT_ID AS CHECKBOX";

		$extra['columns_before'] = [ 'CHECKBOX' => MakeChooseCheckbox(
			// @since RosarioSIS 11.5 Prevent submitting form if no checkboxes are checked
			( version_compare( ROSARIO_VERSION, '11.5', '<' ) ? 'Y' : 'Y_required' ),
			'',
			'st_arr'
		) ];

		$extra['functions'] = [ 'CHECKBOX' => 'MakeChooseCheckbox' ];
	}

	$extra['link'] = [ 'FULL_NAME' => false ];

	$extra['new'] = true;

	$extra['options']['search'] = false;

	// Call our custom Widget.
	MyWidgets( 'subject' );

	Search( 'student_id', $extra );

	// If student list.
	if ( $_REQUEST['search_modfunc'] === 'list' )
	{
		// SubmitButton is diplayed only if AllowEdit.
		echo '<br /><div class="center">' .
			SubmitButton( dgettext( 'Example', 'Create Subject PDF for Selected Students' ) ) . '</div>';

		echo '</form>';
	}
}


/**
 * My Custom Widgets
 *
 * @example MyWidgets( 'subject' );
 *
 * @global $extra The $extra variable contains the options for the Search function
 * @global $_ROSARIO sets $_ROSARIO['SearchTerms']
 *
 * @param string $item 'subject' (Subject Widget).
 */
function MyWidgets( $item )
{
	global $extra,
		$_ROSARIO;

	switch ( $item )
	{
		// Subject Widget.
		case 'subject':

			// If subject selected.
			if ( ! empty( $_REQUEST['subject_id'] ) )
			{
				// Limit student search to subject.
				$extra['WHERE'] =  " AND exists(SELECT ''
					FROM schedule sch, course_periods cp, courses c
					WHERE sch.STUDENT_ID=s.STUDENT_ID
					AND cp.SYEAR=ssm.SYEAR
					AND sch.SYEAR=ssm.SYEAR
					AND sch.MARKING_PERIOD_ID IN (" . GetAllMP( UserMP() ) . ")
					AND cp.COURSE_PERIOD_ID=sch.COURSE_PERIOD_ID
					AND cp.COURSE_ID=c.COURSE_ID
					AND c.SUBJECT_ID='" . (int) $_REQUEST['subject_id'] . "')";

				// Add SearchTerms.
				if ( empty( $extra['NoSearchTerms'] ) )
				{
					$subject_RET = DBGet( "SELECT TITLE
						FROM course_subjects
						WHERE SUBJECT_ID='" . (int) $_REQUEST['subject_id'] . "'
						AND SCHOOL_ID='" . UserSchool() . "'
						AND SYEAR='" . UserSyear() . "'" );

					$_ROSARIO['SearchTerms'] = issetVal( $_ROSARIO['SearchTerms'], '' );

					$_ROSARIO['SearchTerms'] .= '<b>' . _( 'Subject' ) . ':</b> ' .
						$subject_RET[1]['TITLE'];

					$_ROSARIO['SearchTerms'] .= '<input type="hidden" id="subject" name="subject" value="' . ( function_exists( 'AttrEscape' ) ? AttrEscape( $subject_RET[1]['TITLE'] ) : htmlspecialchars( $subject_RET[1]['TITLE'], ENT_QUOTES ) ) . '" /><br />';
				}
			}

			// Get subjects.
			$subjects_RET = DBGet( "SELECT SUBJECT_ID,TITLE
				FROM course_subjects WHERE SCHOOL_ID='" . UserSchool() . "'
				AND SYEAR='" . UserSyear() . "'" );

			$subjects_options = [];

			// Create select options with subjects.
			foreach ( (array) $subjects_RET as $subject )
			{
				$subjects_options[ $subject['SUBJECT_ID'] ] = $subject['TITLE'];
			}

			$allow_na = false;

			// Add Widget to Search.
			$extra['search'] = '<tr><td>' . _( 'Subject' ) . '</td>
				<td>' . SelectInput( '', 'subject_id', '', $subjects_options, $allow_na, 'required' ) .
				'</td></tr>';

		break;
	}
}

��          |      �             !     5     B     P  (   ^     �     �     �     �     �  *   �  �  
     �                 7   ,     d     l       %   �  "   �  .   �               
                          	                  # of Administrators # of Parents # of Students # of Teachers Create Subject PDF for Selected Students Example Example Resource Example Widget Example config value label Example module Setup The configuration value has been modified. Project-Id-Version: Example module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-01 18:59+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-SearchPath-0: .
 # de administradores # de padres # de estudiantes # de docentes Crear el PDF Materia para los estudiantes seleccionados Ejemplo Recurso de ejemplo Widget de ejemplo Ejemplo de nombre del valor de config Configuración del módulo Ejemplo El valor de configuración ha sido modificado. 
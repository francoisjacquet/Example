��          |      �             !     5     B     P  (   ^     �     �     �     �     �  *   �  �  
     �     �            7   *     b     j     |  "   �     �  -   �               
                          	                  # of Administrators # of Parents # of Students # of Teachers Create Subject PDF for Selected Students Example Example Resource Example Widget Example config value label Example module Setup The configuration value has been modified. Project-Id-Version: Example module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-01 19:00+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 2.4.2
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 # d'administrateurs # de parents # d'élèves # de professeurs Créer le PDF matière pour les élèves sélectionnés Exemple Ressource exemple Widget exemple Exemple de nom de valeur de config Paramétrage du module Exemple La valeur de configuration a été modifiée. 